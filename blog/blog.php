<?php require_once 'header.php';
	use App\classes\Helper;
?>
	<head>
        <!-- Meta Tags -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="keywords" content="Blog posts">
		<meta name="description" content="Blog posts">
		<meta name='copyright' content=''>
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		
		<!-- Title -->
        <title>Digital Pulz - Blog</title>
		
		<!-- Favicon -->
        <link rel="icon" href="img/logo.png">
		
		<!-- Google Fonts -->
		<link href="https://fonts.googleapis.com/css?family=Poppins:200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<!-- Nice Select CSS -->
		<link rel="stylesheet" href="css/nice-select.css">
		<!-- Font Awesome CSS -->
        <link rel="stylesheet" href="css/font-awesome.min.css">
		<!-- icofont CSS -->
        <link rel="stylesheet" href="css/icofont.css">
		<!-- Slicknav -->
		<link rel="stylesheet" href="css/slicknav.min.css">
		<!-- Owl Carousel CSS -->
        <link rel="stylesheet" href="css/owl-carousel.css">
		<!-- Datepicker CSS -->
		<link rel="stylesheet" href="css/datepicker.css">
		<!-- Animate CSS -->
        <link rel="stylesheet" href="css/animate.min.css">
		<!-- Magnific Popup CSS -->
        <link rel="stylesheet" href="css/magnific-popup.css">
		
		<!-- Medipro CSS -->
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="style.css">
        <link rel="stylesheet" href="css/responsive.css">
		
		<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
		
		<script async>(function(w, d) { w.CollectId = "5f606819e663330fdf5dbaeb"; var h = d.head || d.getElementsByTagName("head")[0]; var s = d.createElement("script"); s.setAttribute("type", "text/javascript"); s.setAttribute("src", "https://collectcdn.com/launcher.js"); h.appendChild(s); })(window, document);</script>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-160164333-1"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());

			gtag('config', 'UA-160164333-1');
		</script>
		<style>
		* {
		  box-sizing: border-box;
		}

		/* Add a card effect for articles */
		.card {
		   background-color: white;
		   padding: 20px;
		   margin-top: 20px;
		}

		/* Clear floats after the columns */
		.row:after {
		  content: "";
		  display: table;
		  clear: both;
		}

		/* Responsive layout - when the screen is less than 800px wide, make the two columns stack on top of each other instead of next to each other */
		@media screen and (min-width: 992) {
		  .leftcolumn, .rightcolumn {   
			width: 100%;
			padding-left: 10px;
		  }
		}
		.bg {
		  /* The image used */
		  background-image: url("img/blog_bg.jpg");

		  /* Full height */
		  height: 100%; 

		  /* Center and scale the image nicely */
		  background-position: center;
		  background-repeat: no-repeat;
		  background-size: cover;
		}
		
		#more {display: none;}
		#more1 {display: none;}
		#more2 {display: none;}
		#more3 {display: none;}
		
		@media only screen and (min-width: 768px) and (max-width: 991px){

			/*div.mobile{*/
			/*	display: none;*/
			/*}*/
			.mobile-image{
				height: 350px;
			}
			.mobile-image1{
				height: 200px;
				width: 400px;
			}

		}
		@media only screen and (min-width: 100px) and (max-width: 768px) {

			/*.logo {*/
			/*	max-width:30%;*/
			/*	max-height:10%;*/
	
			/*}*/
			div.desktop{
				display: none;
			}
			#mobile(display:block;);
			
			/*.mobile-image{*/
			/*	height: 50px;*/
			/*}*/
			.mobile-image1{
				height: 100px;
				width: 200px;
			}
			
		}
		
		@font-face {
			font-family: "Raleway-Regular";
			src: url("fonts/Raleway-Regular.ttf");
		}
		@font-face {
			font-family: "Raleway-Bold";
			src: url("fonts/Raleway-Bold.ttf");
		}
		@font-face {
			font-family: "Raleway-Medium";
			src: url("fonts/Raleway-Medium.ttf");
		}
		@font-face {
			font-family: "Raleway-Light";
			src: url("fonts/Raleway-Light.ttf");
		}
		@font-face {
			font-family: "DMSans-Medium";
			src: url("fonts/DMSans-Medium.ttf");
		}
		@font-face {
			font-family: "DMSans-Regular";
			src: url("fonts/DMSans-Regular.ttf");
		}
		
		h1{ font-family: Raleway-Bold; text-transform: uppercase; color: #00247D; }
		h2{ font-family: Raleway-Medium; color: #00BD9D;}
		h3{ font-family: DMSans-Medium; color: #323133;}
		h4{ font-family: DMSans-Regular;}
		p1{ font-family: Raleway-Light;}
		p2{ font-family: DMSans-Regular; color: #CF142B;}
		p3{ font-family: Raleway-Light;}
		h5,h6,h7,li,p,ul,ol,a,textarea,button { font-family: Raleway-Regular;}
		blockquote { font-family: Raleway-Regular;}
		pre { font-family: Raleway-Regular;}
		
		.fa {
			padding: 10px;
			font-size: 15px;
			width: 32px;
			text-align: center;
			text-decoration: none;
			margin: 5px 2px;
		}

		.fa:hover {
			opacity: 0.7;
		}

		.fa-facebook {
			background: #3B5998;
			color: white;
		}
		.fa-linkedin {
			background: #007bb5;
			color: white;
		}

		.fa-youtube {
			background: #bb0000;
		  color: white;
		}

		.fa-instagram {
			background: #125688;
			color: white;
		}
		.fa:hover {
			opacity: 0.7;
		}
		
	</style>
	</head>
		
		<div class="col-lg-9 col-md-12 col-sm-12 col-xs-12">
        <div class="page-wrapper">
            <?php
            if(isset($_GET['catwisepost']) && isset($_GET['id'])){
                $id = $_GET['id'];
                $catWisepost = \App\classes\Post::categoryWisePost($id);
                if($catWisepost == false){
                    echo '<h1 class="text-center">Not Avilable  !!</h1>';
                }else{
                    while ($catWisepostRow = mysqli_fetch_assoc($catWisepost)) { ?>
                        <div class="blog-list clearfix">
                            <div class="blog-box row">
                                <div class="col-md-4">
                                    <div class="post-media">
                                        <a href="singlepage.php?id=<?= catWisepostRow['id'] ?>" title="">
                                            <img src="uploads/<?=$catWisepostRow['image'] ?>" alt="" class="img-fluid w-100">
                                            <div class="hovereffect"></div>
                                        </a>
                                    </div><!-- end media -->
                                </div><!-- end col -->

                                <div class="blog-meta big-meta col-md-8">
                                    <h4><a href="singlepage.php?id=<?= $catWisepostRow['id'] ?>" title=""><?= $catWisepostRow['title'] ?></a></h4>
                                    <p><?php
                                        $txt = $catWisepostRow['content'];
                                        echo   \App\classes\Helper::textShort("$txt",250);
                                        ?></p>
                                    <small class="firstsmall"><a class="bg-orange" href="singlepage.php?id=<?= $catWisepostRow['id'] ?>" title="Category"><?= $catWisepostRow['category_name'] ?></a></small>
                                    <small><a  title="Date and Time"><?= $catWisepostRow['date'] ?></a></small>
                                    <small><a  title="Author or Media"><?= $catWisepostRow['admin'] ?></a></small>
                                </div><!-- end meta -->
                            </div><!-- end blog-box -->
                        </div>
                        <hr>
                    <?php } }
            }
            #---------------------
            elseif (isset($_GET['search-btn'])){
                $searchContent = $_GET['search'];
                $var = \App\classes\Post::searchPost($searchContent);
                if($var == false){
                    echo '<h1 class="text-center">Not Avilable Post !!</h1>';
                }else{
                    while ($rowSearch = mysqli_fetch_assoc($var)){ ?>
                        <div class="blog-list clearfix">
                            <div class="blog-box row">
                                <div class="col-md-4">
                                    <div class="post-media">
                                        <a href="singlepage.php?id=<?= $rowSearch['id'] ?>" title="">
                                            <img src="uploads/<?= $rowSearch['image'] ?>" alt="" class="img-fluid w-100">
                                            <div class="hovereffect"></div>
                                        </a>
                                    </div><!-- end media -->
                                </div><!-- end col -->

                                <div class="blog-meta big-meta col-md-8">
                                    <h4><a href="singlepage.php?id=<?= $rowSearch['id'] ?>" title=""><?= $rowSearch['title'] ?></a></h4>
                                    <p><?php
                                        $txt = $rowSearch['content'];
                                        echo   \App\classes\Helper::textShort("$txt",250);
                                        ?></p>
                                    <small class="firstsmall"><a class="bg-orange" href="singlepage.php?id=<?= $rowSearch['id'] ?>" title="Category"><?= $rowSearch['category_name'] ?></a></small>
                                    <small><a  title="Date and Time"><?= $rowSearch['date'] ?></a></small>
                                    <small><a  title="Author or Media"><?= $rowSearch['admin'] ?></a></small>
                                </div><!-- end meta -->
                            </div><!-- end blog-box -->
                        </div>
                        <hr>

                    <?php  }
                }
            }
            # ------------------------------------------------
            else{
            ?>
            <?php
				$ob = new \App\classes\Site();
				$a = $ob->display();
				$siteData = mysqli_fetch_assoc($a);
				$skip = 0;
				$take = $siteData['postdisplay'];
				$page = 1;
				if (isset($_GET['page'])) {
					$page = $_GET['page'];
					$skip = ( $page - 1 ) * $take;
				}
				$totalPost = \App\classes\Post::countActivePost();
				$totalPage = ceil($totalPost/$take);
				if($totalPage < $page)
				{
					header("location:login.php");
				}
				$sql = "SELECT blog.*, categories.category_name FROM blog INNER JOIN categories ON blog.cat_id = categories.id ORDER BY id DESC LIMIT $skip,$take ";
				$post = \App\classes\Post::pagination($sql);
				while ($row = mysqli_fetch_assoc($post)){
					?>
					<div class="blog-list clearfix">
						<div class="blog-box row">
							<div class="col-md-4">
								<div class="post-media">
									<a href="singlepage.php?id=<?= $row['id'] ?>" title="">
										<img src="uploads/<?= $row['image'] ?>" alt="" class="img-fluid w-100">
										<div class="hovereffect"></div>
									</a>
								</div><!-- end media -->
							</div><!-- end col -->

							<div class="blog-meta big-meta col-md-8">
								<h4><a href="singlepage.php?id=<?= $row['id'] ?>" title=""><?= $row['title'] ?></a></h4>
								<p><?php
									$txt = $row['content'];
									echo   \App\classes\Helper::textShort("$txt",250);
									?></p>
								<small class="firstsmall"><a class="bg-orange" href="singlepage.php?id=<?= $row['id'] ?>" title="Category"><?= $row['category_name'] ?></a></small>
								<small><a  title="Date and Time"><?= $row['date'] ?></a></small>
								<small><a  title="Author or Media"><?= $row['admin'] ?></a></small>
							</div><!-- end meta -->
						</div><!-- end blog-box -->
					</div>
					<hr>

				<?php } ?>

            <!-- end blog-list -->
        </div><!-- end page-wrapper -->
        <div class="row text-center">
            <div class="col-md-12">
                <nav aria-label="Page navigation">
                    <ul class="pagination justify-content-between">
                        <li class="page-item">
                            <?php if ($page>1){ ?>
                                <a href="index.php?page=<?=$page - 1;?>" class="page-link info"><i class="fa fa-chevron-left" style="margin-right: 2px;"></i> Prev</a>
                            <?php }?>
                        <li class="page-item">
                            <?php if($totalPage > $page) { ?>
                                <a href="index.php?page=<?=$page + 1;?>"class="page-link btn info">Next <i class="fa fa-chevron-right" style="margin-left: 2px;"></i></a>
                            <?php } ?>
                        </li>
                    </ul>
                </nav>
            </div><!-- end col -->
            <?php } ?>
        </div><!-- end row -->

	</div><!-- end col -->
	<?php
		require_once 'sidebar.php';
	?>

	<?php
		require_once 'footer.php';
	?>
	
	<!-- jquery Min JS -->
        <script src="js/jquery.min.js"></script>
		<!-- jquery Migrate JS -->
		<script src="js/jquery-migrate-3.0.0.js"></script>
		<!-- jquery Ui JS -->
		<script src="js/jquery-ui.min.js"></script>
		<!-- Easing JS -->
        <script src="js/easing.js"></script>
		<!-- Color JS -->
		<script src="js/colors.js"></script>
		<!-- Popper JS -->
		<script src="js/popper.min.js"></script>
		<!-- Bootstrap Datepicker JS -->
		<script src="js/bootstrap-datepicker.js"></script>
		<!-- Jquery Nav JS -->
        <script src="js/jquery.nav.js"></script>
		<!-- Slicknav JS -->
		<script src="js/slicknav.min.js"></script>
		
		<!-- Niceselect JS -->
		<script src="js/niceselect.js"></script>
		<!-- Tilt Jquery JS -->
		<script src="js/tilt.jquery.min.js"></script>
		<!-- Owl Carousel JS -->
        <script src="js/owl-carousel.js"></script>
		<!-- counterup JS -->
		<script src="js/jquery.counterup.min.js"></script>
		<!-- Steller JS -->
		<script src="js/steller.js"></script>
		<!-- Wow JS -->
		<script src="js/wow.min.js"></script>
		<!-- Magnific Popup JS -->
		<script src="js/jquery.magnific-popup.min.js"></script>
		<!-- Counter Up CDN JS -->
		<script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
		<!-- Bootstrap JS -->
		<script src="js/bootstrap.min.js"></script>
		<!-- Main JS -->
		<script src="js/main.js"></script>
<?php

?>