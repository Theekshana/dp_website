<?php 
require_once 'vendor/autoload.php';
use App\classes\Post;
use App\classes\Site;
$ob = Site::display();
$siteData = mysqli_fetch_assoc($ob);
#$post = Post::showActivelPost();
$populer = Post::showPopulerlPost();
$page = explode('/',$_SERVER['PHP_SELF']);
$page = end($page);
$title = '';
if($page == 'login.php'){
    $title = 'Home';
}
elseif ($page == 'contact.php'){
    $title = 'Contact';
}
use App\classes\Session;
if (isset($_POST['msg-btn'])) {
    $ob = new \App\classes\Mail();
    $ob->sendMail($_POST);
}
?>

<div class="col-lg-12 border border-primary">
    <h3>Send us a message</h3><br>
    <span style="color: #0b2e13"><?= Session::get("succesMail") ?></span>
    <span style="color: red"><?= Session::get("failMail") ?></span>
    <form class="form-wrapper" action="" method="post" >
        <div class="form-group">
            <input type="text" required class="form-control" placeholder="Your name" name="name" style="margin-bottom: 0px;">
            <span style="margin: 0px;color: red"><?= Session::get("emptyName") ?></span>
        </div>
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Email address" name="email" style="margin-bottom: 0px;>
        <span style="margin: 0px;color: red"><?= Session::get("emptyEmail")?></span>
        </div>
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Phone" name="phone" style="margin-bottom: 0px;>
            <span style="margin: 0px;color: red"><?= Session::get("emptyPhone") ?></span>
        </div>
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Subject" name="subject" style="margin-bottom: 0px;>
            <span style="margin: 0px;color: red"><?= Session::get("emptySubject") ?></span>
        </div>
        <textarea class="form-control" placeholder="Your message" name="message" ></textarea>
        <span style="margin: 0px;color: red"><?= Session::get("emptymsg") ?></span><br>
        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="msg-btn">Send <i class="fa fa-envelope-open-o"></i></button>
        </div>

    </form>
</div>
<?php
Session::unsetSession("emptyName");
Session::unsetSession("emptyEmail");
Session::unsetSession("emptyPhone");
Session::unsetSession("emptySubject");
Session::unsetSession("succesMail");
Session::unsetSession("failMail");
?>
