</div><!-- end row -->
</div><!-- end container -->
</section>

<?php
use App\classes\Site;
$ob = Site::displaySocialLink();
$data = mysqli_fetch_assoc($ob);
?>
<!-- Footer Area -->
		<footer id="footer" class="footer ">
			<!-- Footer Top -->
			<div class="footer-top">
				<div class="container">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-12">
							<div class="single-footer">
								<h2><img src="img/logo2.png" alt="index.html" width="20%" height="5%"></h2>
								<p>Digital Pulz (Pvt) Ltd. © <br>We make healthcare professionals' life easy.</p>
								<!-- Social -->
								<ul class="social">
									<li><a href="https://www.facebook.com/DigitalPulz/" target="_blank"><i class="icofont-facebook"></i></a></li>
									<li><a href="https://www.linkedin.com/company/digitalpulz/" target="_blank"><i class="icofont-linkedin"></i></a></li>
									<li><a href="https://www.youtube.com/channel/UC0SZYSjEh2ZwwC6MnyEiTQw" target="_blank"><i class="icofont-youtube"></i></a></li>
									<li><a href="https://instagram.com/digital_pulz?igshid=1b8futsxvy27b" target="_blank"><i class="icofont-instagram"></i></a></li>
								</ul>
								<!-- End Social -->
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-12">
							<div class="single-footer f-link">
								<h2>Quick Links</h2>
								<div class="row">
									<div class="col-lg-6 col-md-6 col-12">
										<ul>
											
											<li><a href="https://www.digitalpulz.com/aboutus.html"><i class="fa fa-caret-right" aria-hidden="true"></i>About Us</a></li>
											<li><a href="https://www.digitalpulz.com/contact.html"><i class="fa fa-caret-right" aria-hidden="true"></i>Contact Us</a></li>
												
										</ul>
									</div>
								
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<!--/ End Footer Top -->
			<!-- Copyright -->
			<div class="copyright">
				<div class="container">
					<div class="row">
						<div class="col-lg-12 col-md-12 col-12">
							<div class="copyright-content">
								<p>© Digital Pulz (Pvt) Ltd, 2021 All rights reserved. <a href="privacy-policy.html">Privacy Policy</a> | <a href="terms-and-conditions.html">Terms & Conditions</a></p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--/ End Copyright -->
		</footer>
	<!--/ End Footer Area -->

</div><!-- end wrapper -->

<!-- Core JavaScript
================================================== -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/tether.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/custom.js"></script>

</body>
</html>