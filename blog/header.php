<?php
require_once 'vendor/autoload.php';
use App\classes\Post;
use App\classes\Site;
$ob = Site::display();
$siteData = mysqli_fetch_assoc($ob);
#$post = Post::showActivelPost();
$populer = Post::showPopulerlPost();
$page = explode('/',$_SERVER['PHP_SELF']);
$page = end($page);
$title = '';
if($page == 'login.php'){
    $title = 'Home';
}
elseif ($page == 'contact.php'){
    $title = 'Contact';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Meta Tags -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="keywords" content="Blog posts">
		<meta name="description" content="Blog posts">
		<meta name='copyright' content=''>
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		
		<!-- Title -->
        <title>Digital Pulz - Blog</title>
		
		<!-- Favicon -->
        <link rel="icon" href="img/logo.png">
		
		<!-- Google Fonts -->
		<link href="https://fonts.googleapis.com/css?family=Poppins:200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<!-- Nice Select CSS -->
		<link rel="stylesheet" href="css/nice-select.css">
		<!-- Font Awesome CSS -->
        <link rel="stylesheet" href="css/font-awesome.min.css">
		<!-- icofont CSS -->
        <link rel="stylesheet" href="css/icofont.css">
		<!-- Slicknav -->
		<link rel="stylesheet" href="css/slicknav.min.css">
		<!-- Owl Carousel CSS -->
        <link rel="stylesheet" href="css/owl-carousel.css">
		<!-- Datepicker CSS -->
		<link rel="stylesheet" href="css/datepicker.css">
		<!-- Animate CSS -->
        <link rel="stylesheet" href="css/animate.min.css">
		<!-- Magnific Popup CSS -->
        <link rel="stylesheet" href="css/magnific-popup.css">
		
		<!-- Medipro CSS -->
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="style.css">
        <link rel="stylesheet" href="css/responsive.css">
		
		<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
		
		<script async>(function(w, d) { w.CollectId = "5f606819e663330fdf5dbaeb"; var h = d.head || d.getElementsByTagName("head")[0]; var s = d.createElement("script"); s.setAttribute("type", "text/javascript"); s.setAttribute("src", "https://collectcdn.com/launcher.js"); h.appendChild(s); })(window, document);</script>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-160164333-1"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());

			gtag('config', 'UA-160164333-1');
		</script>
		<style>
		* {
		  box-sizing: border-box;
		}

		/* Add a card effect for articles */
		.card {
		   background-color: white;
		   padding: 20px;
		   margin-top: 20px;
		}

		/* Clear floats after the columns */
		.row:after {
		  content: "";
		  display: table;
		  clear: both;
		}


		
		@font-face {
			font-family: "Raleway-Regular";
			src: url("fonts/Raleway-Regular.ttf");
		}
		@font-face {
			font-family: "Raleway-Bold";
			src: url("fonts/Raleway-Bold.ttf");
		}
		@font-face {
			font-family: "Raleway-Medium";
			src: url("fonts/Raleway-Medium.ttf");
		}
		@font-face {
			font-family: "Raleway-Light";
			src: url("fonts/Raleway-Light.ttf");
		}
		@font-face {
			font-family: "DMSans-Medium";
			src: url("fonts/DMSans-Medium.ttf");
		}
		@font-face {
			font-family: "DMSans-Regular";
			src: url("fonts/DMSans-Regular.ttf");
		}
		
		h1{ font-family: Raleway-Bold; text-transform: uppercase; color: #00247D; }
		h2{ font-family: Raleway-Medium; color: #00BD9D;}
		h3{ font-family: DMSans-Medium; color: #323133;}
		h4{ font-family: DMSans-Regular;}
		p1{ font-family: Raleway-Light;}
		p2{ font-family: DMSans-Regular; color: #CF142B;}
		p3{ font-family: Raleway-Light;}
		h5,h6,h7,li,p,ul,ol,a,textarea,button { font-family: Raleway-Regular;}
		blockquote { font-family: Raleway-Regular;}
		pre { font-family: Raleway-Regular;}
		
		.fa {
			padding: 10px;
			font-size: 15px;
			width: 32px;
			text-align: center;
			text-decoration: none;
			margin: 5px 2px;
		}

		.fa:hover {
			opacity: 0.7;
		}

		.fa-facebook {
			background: #3B5998;
			color: white;
		}
		.fa-linkedin {
			background: #007bb5;
			color: white;
		}

		.fa-youtube {
			background: #bb0000;
		  color: white;
		}

		.fa-instagram {
			background: #125688;
			color: white;
		}
		.fa:hover {
			opacity: 0.7;
		}
		
	</style>
	</head>
	<body>

		<!-- Header Area -->
		<header class="header" >

			<div class="topbar desktop">
				<!-- <div class="container"> -->
					<div class="row col-md-12 ">
						<div class="col-md-1 "></div>
						<div class="col-md-3 desktop">
							<!-- Contact -->
							<button class="btn-sm btn-info text-justify-center" style="height: 26px; width: 250px;"><a  href="demo.html" >Request a Demo</a></button>
							<!-- End Contact -->
						</div>
						<div class="col-md-1 "></div>
						<div class="col-md-6 ">
							<!-- Top Contact -->
							<ul class="top-contact ">
								<li style="position: relative;"><i class="fa fa-phone"></i>(+94) 76 095 6050</li>
								<li style="position: relative;"><i class="fa fa-envelope"></i><a href="mailto:info@digitalpulz.com">info@digitalpulz.com</a></li>
							</ul>
							<!-- End Top Contact -->
						</div>
						<div class="col-md-1 "></div>
					</div>

			</div>
			<!-- End Topbar -->
			<!-- Header Inner -->
			<div class="header-inner">
				<div class="container">
					<div class="inner">
						<div class="row">
							<div class="col-lg-3 col-md-2 col-12" >
								<!-- Start Logo -->
								<div class="logo">
									<a href="https://www.digitalpulz.com/index.html"><img src="img/logo1.png" alt="index.html" width="60%" height="20%" class="logoImage"></a>
								</div>
								<!-- End Logo -->
								<!-- Mobile Nav -->
								<div class="mobile-nav"></div>
								<!-- End Mobile Nav -->
							</div>
							<div class="col-lg-9 col-md-10 col-12" style="height: 50px;">
								<!-- Main Menu -->
								<div class="main-menu">
									<nav class="navigation">
										<ul class="nav menu">
											<li><a href="https://www.digitalpulz.com/">Home </a></li>
											<li><a href="#">Products <i class="icofont-rounded-down"></i></a>
												<ul class="dropdown">
													<li><a href="https://www.digitalpulz.com/hms.html">Hospital Management Software</a></li>
													<li><a href="https://www.digitalpulz.com/cms.html">Clinic Management System</a></li>
													<li><a href="https://www.digitalpulz.com/vcms.html">Veterinary Clinic Management Software</a></li>

												</ul>
											</li>
											<li><a href="https://www.digitalpulz.com/features.html">Features</a></li>
											<li><a href="https://www.digitalpulz.com/our-process.html">Process</a></li>
											<li><a href="#">Company <i class="icofont-rounded-down"></i></a>
												<ul class="dropdown">
													<li><a href="https://www.digitalpulz.com/aboutus.html">About Us</a></li>
													<li><a href="https://www.digitalpulz.com/our-initiators.html">Our Initiators</a></li>
													<li><a href="https://www.digitalpulz.com/team.html">Team</a></li>
												</ul>
											</li>
											<li><a href="https://www.digitalpulz.com/contact.html">Contact Us</a></li>
											<li><a href="#">Customer Portal <i class="icofont-rounded-down"></i></a>
												<ul class="dropdown">
													<li><a href="https://www.digitalpulz.com/demo.html">Demo</a></li>
													<li><a href="https://www.digitalpulz.com/customerportal.html">Complaints & Suggestions</a></li>
													<li><a href="https://finance.digitalpulzhealth.com/Payment">Online Payments</a></li>
												</ul>
											</li>
											<li class="active"><a href="https://www.digitalpulz.com/blog/blog.php">Blog</a></li>
										</ul>
									</nav>
								</div>
								<!--/ End Main Menu -->
							</div>
							
						</div>
					</div>
				</div>
			</div>
			<!--/ End Header Inner -->
		</header>
        <div class="container">
            <div class="row">
                
        <script src="js/jquery.min.js"></script>
        <link rel="preload" href="js/jquery.min.js" as="script">
		<!-- jquery Migrate JS -->
		<script src="js/jquery-migrate-3.0.0.js"></script>
		<link rel="preload" href="js/jquery-migrate-3.0.0.js" as="script">
		<!-- jquery Ui JS -->
		<script src="js/jquery-ui.min.js"></script>
		<link rel="preload" href="js/jquery-ui.min.js" as="script">
		<!-- Easing JS -->
        <script src="js/easing.js"></script>
        <link rel="preload" href="js/easing.js" as="script">
		<!-- Color JS -->
		<script src="js/colors.js"></script>
		<link rel="preload" href="js/colors.js" as="script">
		<!-- Popper JS -->
		<script src="js/popper.min.js"></script>
		<link rel="preload" href="js/popper.min.js" as="script">
		<!-- Bootstrap Datepicker JS -->
		<script src="js/bootstrap-datepicker.js"></script>
		<link rel="preload" href="js/bootstrap-datepicker.js" as="script">
		<!-- Jquery Nav JS -->
        <script src="js/jquery.nav.js"></script>
        <link rel="preload" href="js/jquery.nav.js" as="script">
		<!-- Slicknav JS -->
		<script src="js/slicknav.min.js"></script>
		<link rel="preload" href="js/slicknav.min.js" as="script">
	
		<!-- Niceselect JS -->
		<script src="js/niceselect.js"></script>
		<link rel="preload" href="js/niceselect.js" as="script">
		<!-- Tilt Jquery JS -->
		<script src="js/tilt.jquery.min.js"></script>
		<link rel="preload" href="js/tilt.jquery.min.js" as="script">
		<!-- Owl Carousel JS -->
        <script src="js/owl-carousel.js"></script>
        <link rel="preload" href="js/owl-carousel.js" as="script">
		<!-- counterup JS -->
		<script src="js/jquery.counterup.min.js"></script>
		<link rel="preload" href="js/jquery.counterup.min.js" as="script">
		<!-- Steller JS -->
		<script src="js/steller.js"></script>
		<link rel="preload" href="js/steller.js" as="script">
		<!-- Wow JS -->
		<script src="js/wow.min.js"></script>
		<link rel="preload" href="js/wow.min.js" as="script">
		<!-- Magnific Popup JS -->
		<script src="js/jquery.magnific-popup.min.js"></script>
		<link rel="preload" href="js/jquery.magnific-popup.min.js" as="script">
		<!-- Counter Up CDN JS -->
		<script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
		<link rel="preload" href="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js" as="script">
		<!-- Bootstrap JS -->
		<script src="js/bootstrap.min.js"></script>
		<link rel="preload" href="js/bootstrap.min.js" as="script">
		<!-- Main JS -->
		<script src="js/main.js"></script>
		<link rel="preload" href="js/main.js" as="script">
		
		</body>
	</html>
