<?php
	require_once 'header.php';
?>
	<!-- Start Contact Us -->
		<section class="contact-us section" style="margin-top: -40px;">
			<div class="container">
				<div class="section-title" style="text-align: center;">
					<h2 style="margin-top: -50px;" align="center">Contact Us </h2>
					<img src="img/section-img.png" alt="#" hidden>
				</div>
				<div class="row">
					<div class="col-lg-6 " style="margin-top: -10px;">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2760.5498606594547!2d79.97259692428123!3d6.91544366964524!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ae257f4de2b1437%3A0xc0549b4c61a733ed!2sDigital%20Pulz!5e0!3m2!1sen!2slk!4v1606747979234!5m2!1sen!2slk" width="100%" height="370px" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
					</div>
					<div class="col-lg-6">
						<?php
							require_once 'blog/message.php';
						?>
					</div>
				</div>
				<div class="contact-info">
					<div class="row">
						<!-- single-info -->
						<div class="col-lg-4 col-12 ">
							<div class="single-info">
								<i class="icofont icofont-ui-call"></i>
								<div class="content">
									<h3 style="font-size: 14px">Mobile: (+94) 76 095 6050</h3>
									<h3 style="font-size: 14px">Phone: (+94) 11 555 1990</h3>
									<h3 style="font-size: 14px">info@digitalpulz.com</h3>
								</div>
							</div>
						</div>
						<!--/End single-info -->
						<!-- single-info -->
						<div class="col-lg-4 col-12 ">
							<div class="single-info">
								<i class="icofont-google-map"></i>
								<div class="content">
									<h3 style="font-size: 14px">C/o. Sri Lanka Technology Incubator(Pvt.)Ltd., SLIIT Malabe Campus, </h3>
									<h3 style="font-size: 14px">New Kandy Road, Malabe</h3>
								</div>
							</div>
						</div>
						<!--/End single-info -->
						<!-- single-info -->
						<div class="col-lg-4 col-12 ">
							<div class="single-info">
								<i class="icofont icofont-wall-clock"></i>
								<div class="content">
									<h3>Mon - Fri:<br> 8am - 5pm</h3>
									
								</div>
							</div>
						</div>
						<!--/End single-info -->
					</div>
				</div>
			</div>
		</section>
		<!--/ End Contact Us -->
		
  
  
	<!-- Footer Area -->
		<footer id="footer" class="footer ">
			<!-- Footer Top -->
			<div class="footer-top">
				<div class="container">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-12">
							<div class="single-footer">
								<h2><img src="img/logo2.png" alt="index.html" width="20%" height="5%"></h2>
								<p>Digital Pulz (Pvt) Ltd. © <br>We make healthcare professionals' life easy.</p>
								<!-- Social -->
								<ul class="social">
									<li><a href="https://www.facebook.com/DigitalPulz/" target="_blank"><i class="icofont-facebook"></i></a></li>
									<li><a href="https://www.linkedin.com/company/digitalpulz/" target="_blank"><i class="icofont-linkedin"></i></a></li>
									<li><a href="https://www.youtube.com/channel/UC0SZYSjEh2ZwwC6MnyEiTQw" target="_blank"><i class="icofont-youtube"></i></a></li>
									<li><a href="https://instagram.com/digital_pulz?igshid=1b8futsxvy27b" target="_blank"><i class="icofont-instagram"></i></a></li>
								</ul>
								<!-- End Social -->
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-12">
							<div class="single-footer f-link">
								<h2>Quick Links</h2>
								<div class="row">
									<div class="col-lg-6 col-md-6 col-12">
										<ul>
											
											<li><a href="aboutus.html"><i class="fa fa-caret-right" aria-hidden="true"></i>About Us</a></li>
											<li><a href="contact.html"><i class="fa fa-caret-right" aria-hidden="true"></i>Contact Us</a></li>
												
										</ul>
									</div>
								
								</div>
							</div>
						</div>
						</div>
					
					</div>
				</div>
			</div>
			<!--/ End Footer Top -->
			<!-- Copyright -->
			<div class="copyright">
				<div class="container">
					<div class="row">
						<div class="col-lg-12 col-md-12 col-12">
							<div class="copyright-content">
								<p>© Digital Pulz (Pvt) Ltd, 2021 All rights reserved. <a href="privacy-policy.html">Privacy Policy</a> | <a href="terms-and-conditions.html">Terms & Conditions</a></p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--/ End Copyright -->
		</footer>
		<!--/ End Footer Area -->
		
		<!-- jquery Min JS -->
        <script src="js/jquery.min.js"></script>
		<!-- jquery Migrate JS -->
		<script src="js/jquery-migrate-3.0.0.js"></script>
		<!-- jquery Ui JS -->
		<script src="js/jquery-ui.min.js"></script>
		<!-- Easing JS -->
        <script src="js/easing.js"></script>
		<!-- Color JS -->
		<script src="js/colors.js"></script>
		<!-- Popper JS -->
		<script src="js/popper.min.js"></script>
		<!-- Bootstrap Datepicker JS -->
		<script src="js/bootstrap-datepicker.js"></script>
		<!-- Jquery Nav JS -->
        <script src="js/jquery.nav.js"></script>
		<!-- Slicknav JS -->
		<script src="js/slicknav.min.js"></script>
		
		<!-- Niceselect JS -->
		<script src="js/niceselect.js"></script>
		<!-- Tilt Jquery JS -->
		<script src="js/tilt.jquery.min.js"></script>
		<!-- Owl Carousel JS -->
        <script src="js/owl-carousel.js"></script>
		<!-- counterup JS -->
		<script src="js/jquery.counterup.min.js"></script>
		<!-- Steller JS -->
		<script src="js/steller.js"></script>
		<!-- Wow JS -->
		<script src="js/wow.min.js"></script>
		<!-- Magnific Popup JS -->
		<script src="js/jquery.magnific-popup.min.js"></script>
		<!-- Counter Up CDN JS -->
		<script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
		<!-- Bootstrap JS -->
		<script src="js/bootstrap.min.js"></script>
		<!-- Main JS -->
		<script src="js/main.js"></script>

    </body>
</html>